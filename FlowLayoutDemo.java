import java.awt.*;
public class FlowLayoutDemo extends Frame {
    public FlowLayoutDemo(){
    }
    
    public static void main(String[]args){
        FlowLayoutDemo fld = new FlowLayoutDemo();
        fld.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 10));
        fld.add(new Button("One"));
        fld.add(new Button("Two"));
        fld.add(new Button("Three"));
        fld.setSize(100, 100);
        fld.setVisible(true);
    }
}
